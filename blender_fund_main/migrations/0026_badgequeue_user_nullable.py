# Generated by Django 2.1.14 on 2020-02-06 09:32

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ('blender_fund_main', '0025_badgequeue_email_to_user_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='badgerqueuedcall',
            name='user',
            field=models.ForeignKey(help_text='The owner of the badge', null=True,
                                    on_delete=django.db.models.deletion.CASCADE,
                                    to=settings.AUTH_USER_MODEL),
        ),
    ]
