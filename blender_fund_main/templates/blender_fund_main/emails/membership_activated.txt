Dear {{ customer.full_name|default:user.email }},

Your Krita Fund membership was activated! Thanks for your support.

You can always go to {{ link }} to view and update your membership.

Kind regards,

Krita Foundation
