Dear {{ customer.full_name|default:user.email }},

Thank you for joining the Krita Development Fund. You chose to pay
for your {{ membership.level.name }} membership by bank transfer, which
means that we will be waiting for you to perform this transfer.

When paying, please mention the following:

    Krita Fund Membership subs-{{ subscription.id }}

Please send your payment of {{ subscription.price.with_currency_symbol }} to:

    Stichting Krita Foundation
    Korte Assenstraat 11 7411JP
    Deventer, the Netherlands.
    
    Bank Account
    IBAN: NL72INGB0007216397
    BIC: INGBNL2

You can always go to {{ link }} to view and update your membership.

Kind regards,

Krita Foundation
