# Looper – Subscription Management System

Looper is a flexible subscription management system, designed after real-life scenarios.

## Setting up

```
# install libjpeg-turbo-devel, python3-devel and mariadb-devel packages from your repository some dependencies need them.
pip3 install --user poetry
poetry install
poetry run ./manage.py migrate
poetry run ./manage.py loaddata systemuser devfund default_site wagtail
poetry run ./manage.py collectstatic
poetry run ./manage.py collectmedia --noinput
poetry run ./manage.py runserver 8010
```

After logging in via the web interface and Blender ID, use

```
poetry run ./manage.py makesuperuser {your email address}
```

to make yourself an admin.

## CMS

We use [Wagtail](https://wagtail.io/) as CMS for providing access to dynamic content of the website,
such as page titles, calls to action, or entire pages. The CMS is accessible at the `/cms` endpoint.

Note: we are currently using a development version of Wagtail, therefore we need to build the
static assets for the package ourselves. Check the warning in the startup log for more info on
how to do it.

## Braintree integration and HTTPS

It seems even the Braintree sandbox assumes you're using an HTTPS website. The easiest
way to set this up is to use [stunnel](https://stackoverflow.com/a/8025645/875379) to
create a simple HTTPS wrapper around your devserver.

**NOTE**: to make Django aware of the fact that HTTPS is used, set the environment variable
`HTTPS=1` when running `manage.py runserver`.

You can now access https://fund.local:8443/ to reach your dev server via HTTPS.


## Documentation

```
cd docs
poetry run mkdocs serve -a localhost:8080
```


## Testing

Run `poetry run py.test` to run the unit tests.

By default we use the `--reuse-db` option to speed up subsequent test runs.
However, this skips the database migrations; add `--create-db` on the CLI to
recreate the test database and run all the migrations.


## GeoIP

The GeoIP database was downloaded from
[GeoLite2-Country](http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz)


## Deployment

Deployment is done via Docker, see [docker/README.md](docker/README.md) for more info.
