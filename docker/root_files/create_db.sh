#!/bin/sh -e

# This should only be run once, to create the database + users.
mkdir -p -m 700 $MYSQL_DATA
chown -R mysql:mysql $MYSQL_DATA
mysqld --initialize-insecure

echo '-------------------------------------'
echo "Starting MySQL"
mysqld --daemonize

echo '-------------------------------------'
echo "Creating 'blender_fund' database and user"
mysql <<EOT
CREATE DATABASE blender_fund CHARACTER SET=utf8;
CREATE USER 'blender_fund'@'localhost' IDENTIFIED BY 'blender_fund';
GRANT ALL ON blender_fund.* to 'blender_fund'@'localhost';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'blender_fund'@'localhost';
EOT

echo '-------------------------------------'
echo "Setting password for 'root' user"
mysql <<EOT
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('default-root-password');
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'root'@'localhost';
EOT

echo
echo '-------------------------------------'
echo 'Database users were created with trivial passwords'
echo 'Please change their passwords using'
echo "SET PASSWORD FOR 'root'@'localhost' = PASSWORD('new_password');"
echo "SET PASSWORD FOR 'blender_fund'@'localhost' = PASSWORD('new_password');"
echo "FLUSH PRIVILEGES;"
echo '-------------------------------------'
